extern crate glib;
extern crate gio;
extern crate gtk;

use gio::prelude::*;
use gtk::prelude::*;

use gtk::*;

use std::env::args;

fn build_ui(application: &gtk::Application) {
    let glade_src = include_str!("gui.glade");
    let lbrow_template_src = include_str!("lbrow_template.glade");

    let builder = Builder::new_from_string(glade_src);

    let window: ApplicationWindow = builder.get_object("window1").expect("Couldn't get window1");
    window.set_application(Some(application));
    window.set_title("Rust URL Downloader - GTK");

    let lbox_urls: ListBox = builder.get_object("lbox_urls_main").expect("couldnt get list box");

    let btn_add_row: Button = builder.get_object("btn_add_url").expect("couldnt get button");
    btn_add_row.set_label("Add URL");

    

    btn_add_row.connect_clicked(move |_| {
        println!("Add row");
        let lbrow_template_builder = Builder::new_from_string(lbrow_template_src);
        let lbox_row_template: ListBoxRow = lbrow_template_builder.get_object("lbox_row_templates").unwrap();

        lbox_add_item(&lbox_urls, lbox_row_template);
    });

    window.show_all();
} 


fn lbox_add_item(listbox: &ListBox, template_row: ListBoxRow){
    println!("Adding row..");
    
    &listbox.add(&template_row);
    
    &listbox.show_all();
}

fn main() {
    let application = gtk::Application::new(
        Some("com.rhoatech.rust_downloader"),
        Default::default(),
    )
    .expect("Initialization failed...");

    application.connect_activate(|app| {
        build_ui(app);
    });

    application.run(&args().collect::<Vec<_>>());
}
